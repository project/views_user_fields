(function ($) {
  Drupal.behaviors.views_user_fields_conf_select_all_fields = {
    attach: function (context, settings) {
      $('div[id^="edit-views-user-fields"]', context).each(function () {
        var $section = $(this);
        var $checkAll = $('<input type="checkbox">').attr('title', Drupal.t('Check all'));
        $checkAll.click(function () {
          $section.find('input[type="checkbox"]').attr('checked', $(this).is(':checked'));
        });
        var $label =  $section.find('> label').after($checkAll);
      });
    }
  }
}(jQuery));
