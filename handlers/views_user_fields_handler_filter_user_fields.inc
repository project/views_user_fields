<?php

/**
 * @file
 * Views user fields filter handler.
 */
class views_user_fields_handler_filter_user_fields extends views_handler_filter {

  /**
   * Defines handler options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['user_fields_view_display_id'] = array('default' => 'default');
    $options['expose']['contains']['views_user_fields_fields_conf'] = array('default' => 'default');
    $options['expose']['views_user_fields_fields_conf'] = array('default' => 'default');
    return $options;
  }

  function init(&$view, &$options) {
    parent::init($view, $options);
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Allows to select from which display we want to select user fields.
    $uf_options = array();
    foreach ($this->view->display as $display_name => $display) {
      $uf_options[$display_name] = $display->display_title;
    }
    $form['user_fields_view_display_id'] = array(
      '#type' => 'select',
      '#title' => t('User Fields display'),
      '#description' => t('Select the display for which you would like to select user fields.'),
      '#options' => $uf_options,
      '#default_value' => $this->options['user_fields_view_display_id'],
    );
  }

  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    // Cajole views with this nice dummy element
    $form['value'] = array(
      '#type' => 'value',
      '#value' => NULL,
      '#view_name' => $this->view->name,
      '#view_current_display' => $this->view->current_display,
      '#view_target_display' => $this->options['user_fields_view_display_id'],
    );

  }

  function __not_overriding_exposed_form(&$form, &$form_state) {
    // I moved the form elements building here in a form alter because adding
    // form elements in this method behave in some ways I'm tired to discover...
    // @see views_user_fields_form_views_exposed_form_alter().
    if (empty($this->options['exposed'])) {
      return;
    }
    parent::exposed_form($form, $form_state);
  }

  /**
   * Display the filter on the administrative summary
   */
  function admin_summary() {
    $display_title = $this->view->display[$this->options['user_fields_view_display_id']]->display_title;
    return t('for display %vd', array('%vd' => $display_title));
  }

  /**
   * Do nothing in the query.
   */
  public function query() {

  }

}
