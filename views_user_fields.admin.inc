<?php

// Tabledrag behavior code: documentation http://cgit.drupalcode.org/examples/tree/tabledrag_example?h=7.x-1.x

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_conf_form($form, &$form_state, $view_name, $view_display_id) {

  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs = !empty($confs[$view_name][$view_display_id]) ? $confs[$view_name][$view_display_id] : array();
  $confs += array('default' => array(
      'name' => 'Default',
      'id' => 'default',
  ));

  $form['confs'] = array(
    '#tree' => TRUE,
  );
  foreach ($confs as $id => $conf) {
    $conf += array('name' => 'Default');
    $edit_link = l(t('Edit'), current_path() . '/conf/' . $id . '/edit');
    $duplicate_link = l(t('Duplicate'), current_path() . '/conf/' . $id . '/duplicate');
    $structure_copy_link = l(t('Copy structure'), current_path() . '/conf/' . $id . '/structure-copy');
    $delete_link = l(t('Delete'), current_path() . '/conf/' . $id . '/delete');
    if ($id === 'default') {
      $delete_link = '';
    }
    $form['confs'][$id] = array(
      'name' => array(
        '#markup' => t($conf['name']),
      ),
      'id' => array(
        '#type' => 'hidden',
        '#value' => $id,
      ),
      'op_manage' => array(
        '#markup' => l(t('Manage'), "admin/views-user-fields/manage/$view_name/$view_display_id/conf/$id/"),
      ),
      'op_edit' => array(
        '#markup' => $edit_link,
      ),
      'op_duplicate' => array(
        '#markup' => $duplicate_link,
      ),
      'op_structure_copy' => array(
        '#markup' => $structure_copy_link,
      ),
      'op_delete' => array(
        '#markup' => $delete_link,
      ),
    );
  }

  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );

  return $form;
}

function theme_views_user_fields_admin_manage_fields_conf_form($variables) {
  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form['confs']) as $id) {
    $rows[] = array(
      drupal_render($form['confs'][$id]['name']),
      drupal_render($form['confs'][$id]['op_manage']),
      drupal_render($form['confs'][$id]['op_edit']),
      drupal_render($form['confs'][$id]['op_duplicate']),
      drupal_render($form['confs'][$id]['op_structure_copy']),
      drupal_render($form['confs'][$id]['op_delete']),
    );
  }

  $output = theme('table', array(
    'header' => array(t('Conf'), array('data' => t('Operation'), 'colspan' => 5),),
    'rows' => $rows,
  ));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_conf_edit_form($form, &$form_state, $view_name, $view_display_id, $conf_id = NULL) {
  $form['conf_name'] = array(
    '#title' => t('Name'),
    '#description' => t('Name of the fields configuration.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  if ($conf_id === NULL) {
    $form['conf_id'] = array(
      '#type' => 'machine_name',
      '#machine_name' => array(
        'source' => array('conf_name'),
        'exists' => 'views_user_fields_fields_conf_id_exists',
      ),
    );
  }
  else {
    $confs = variable_get('views_user_fields_fields_confs', array());
    $confs = $confs[$view_name][$view_display_id][$conf_id];
    $form['conf_id'] = array(
      '#type' => 'hidden',
      '#value' => $conf_id,
    );
    $form['conf_name']['#default_value'] = $confs['name'];
  }
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/";
  if ($conf_id === NULL) {
    $form = confirm_form($form, 'Add configuration', $form_state['storage']['redirect'], '', t('Save'));
  }
  else {
    $form = confirm_form($form, 'Edit configuration', $form_state['storage']['redirect'], '', t('Save'));
  }
  return $form;
}

/**
 * Machine name #exists callback.
 */
function views_user_fields_fields_conf_id_exists($conf_id, $element, $form_state) {
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs = !empty($confs[$view_name][$view_display_id]) ? $confs[$view_name][$view_display_id] : array();
  return array_key_exists($conf_id, $confs);
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_conf_add_form().
 */
function views_user_fields_admin_manage_fields_conf_edit_form_submit($form, &$form_state) {
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];
  $conf_name = $form_state['values']['conf_name'];
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs[$view_name][$view_display_id][$conf_id]['name'] = $conf_name;
  variable_set('views_user_fields_fields_confs', $confs);
  $form_state['redirect'] = $form_state['storage']['redirect'];
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_conf_duplicate_form($form, &$form_state, $view_name, $view_display_id, $conf_id = NULL) {
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs = $confs[$view_name][$view_display_id][$conf_id];

  $m = t('Duplicate @conf_name configuration', array('@conf_name' => $confs['name']));

  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/";
  $form = confirm_form($form, $m, $form_state['storage']['redirect'], '', $m);
  return $form;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_conf_add_form().
 */
function views_user_fields_admin_manage_fields_conf_duplicate_form_submit($form, &$form_state) {

  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];
  $confs = variable_get('views_user_fields_fields_confs', array());

  $conf_id_duplcate = $conf_id;

  $i = 0;
  while (!empty($confs[$view_name][$view_display_id][$conf_id_duplcate . '_' . $i])) {
    $i++;
  }
  $confs[$view_name][$view_display_id][$conf_id_duplcate . '_' . $i] = $confs[$view_name][$view_display_id][$conf_id];
  $confs[$view_name][$view_display_id][$conf_id_duplcate . '_' . $i]['name'] = $confs[$view_name][$view_display_id][$conf_id]['name'] . ' duplicate';
  variable_set('views_user_fields_fields_confs', $confs);
  $form_state['redirect'] = $form_state['storage']['redirect'];
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_conf_structure_copy_form($form, &$form_state, $view_name, $view_display_id, $conf_id) {
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs_source = $confs[$view_name][$view_display_id][$conf_id];

  $m = t('Copy @conf_name configuration structure', array('@conf_name' => $confs_source['name']));

  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );

  $conf_id_target_options = array();
  foreach ($confs[$view_name][$view_display_id] as $key => $value) {
    if ($key != $conf_id) {
      $conf_id_target_options[$key] = $value['name'];
    }
  }
  $form['conf_id_target'] = array(
    '#type' => 'select',
    '#title' => t('Target configuration'),
    '#description' => t('Select which target configuration you want to copy the structure to (the checked selection will be keep on target).'),
    '#options' => $conf_id_target_options,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/";
  $form = confirm_form($form, $m, $form_state['storage']['redirect'], '', $m);
  return $form;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_conf_add_form().
 */
function views_user_fields_admin_manage_fields_conf_structure_copy_form_submit($form, &$form_state) {

  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];
  $conf_id_target = $form_state['values']['conf_id_target'];
  $confs = variable_get('views_user_fields_fields_confs', array());

  $conf_new = $confs[$view_name][$view_display_id][$conf_id];
  $conf_target = $confs[$view_name][$view_display_id][$conf_id_target];
  $conf_new['name'] = $conf_target['name'];

  foreach ($conf_new['fields'] as $id => $field) {
    if (strpos($id, 'FIELD-') === 0 && !empty($conf_target['fields'][$id])) {
      $conf_new['fields'][$id]['checked'] = $conf_target['fields'][$id]['checked'];
    }
  }

  $confs[$view_name][$view_display_id][$conf_id_target] = $conf_new;

  variable_set('views_user_fields_fields_confs', $confs);
  $form_state['redirect'] = $form_state['storage']['redirect'];
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_conf_delete_form($form, &$form_state, $view_name, $view_display_id, $conf_id) {
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs = $confs[$view_name][$view_display_id][$conf_id];

  $form['message'] = array(
    '#markup' => t('Delete %section_name', array('%section_name' => $confs['name'])),
  );
  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/";
  $form = confirm_form($form, 'Delete', $form_state['storage']['redirect'], '', t('Delete'));
  return $form;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_conf_delete_form().
 */
function views_user_fields_admin_manage_fields_conf_delete_form_submit($form, &$form_state) {
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];
  $confs = variable_get('views_user_fields_fields_confs', array());

  unset($confs[$view_name][$view_display_id][$conf_id]);

  variable_set('views_user_fields_fields_confs', $confs);
  $form_state['redirect'] = $form_state['storage']['redirect'];
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_form($form, &$form_state, $view_name, $view_display_id, $conf_id = 'default') {
  /* @var $view \view */
  // Get fields
  $view = views_get_view($view_name);
  $view->set_display($view_display_id);
  $view->init_handlers();

  // Get sections
  $field_sections = variable_get('views_user_fields_fields_sections', array());
  $field_sections = !empty($field_sections[$view_name][$view_display_id][$conf_id]) ? $field_sections[$view_name][$view_display_id][$conf_id] : array();

  // Get conf
  $confs = variable_get('views_user_fields_fields_confs', array());
  $conf = !empty($confs[$view_name][$view_display_id][$conf_id]) ? $confs[$view_name][$view_display_id][$conf_id] : array();
  $conf += array(
    'name' => t('Default'),
    'fields' => array(),
  );

  $form['title'] = array(
    '#markup' => t('%c fields', array('%c' => !empty($conf['name']) ? $conf['name'] : t('Default'))),
  );

  $weight_for_new = 0;
  $data = array();
  foreach ($view->field as $field_name => $field_handler) {
    /* @var $field_handler views_handler_field_node */
    if (empty($field_handler->options['exclude'])) {
      $id = 'FIELD-' . $field_name;
      if (empty($conf['fields'][$id])) {
        $data[$id] = array(
          'id' => $field_name,
          'pid' => 0,
          'weight' => $weight_for_new++,
          'depth' => 0,
          'type' => 'field',
          'checked' => '0',
        );
      }
      else {
        $data[$id] = $conf['fields'][$id];
      }
      $data[$id]['name'] = $field_handler->label();
      $data[$id]['name'] = $field_handler->label() ? : $field_handler->definition['title'];
    }
  }
  // Add section
  foreach ($conf['fields'] as $id => $section) {
    if (strpos($id, 'SECTION-') === 0) {
      $data[$id] = array(
        'id' => $id,
        'pid' => 0,
        'weight' => $section['weight'],
        'depth' => 0,
        'type' => 'section',
        'checked' => '0',
        'name' => $section['name']
      );
    }
  }

  // Sort
  uasort($data, function($a, $b) {
    if ($a['weight'] == $b['weight']) {
      return 0;
    }
    return ($a['weight'] < $b['weight']) ? -1 : 1;
  });

  // Depth
  $data_bis = array();
  foreach ($data as $id => $item) {
    if (empty($item['pid'])) {
      $data_bis[$id] = $item;
      $data_bis[$id]['depth'] = 0;
      foreach ($data as $cid => $citem) {
        if ($citem['pid'] === $id) {
          $data_bis[$cid] = $citem;
          $data_bis[$cid]['depth'] = 1;
        }
      }
    }
    elseif (empty($data[$item['pid']])) {
      // orphans
      $data_bis[$id] = $item;
      $data_bis[$id]['depth'] = 0;
    }
  }

  $form['views_user_fields'] = array(
    '#tree' => TRUE,
  );

  foreach ($data_bis as $id => $item) {
    $edit_link = '';
    $delete_link = '';
    if (strpos($id, 'SECTION-') === 0) {
      $edit_link = l(t('Edit'), current_path() . '/section-edit/' . $id);
      $delete_link = l(t('Delete'), current_path() . '/section-delete/' . $id);
    }

    $form['views_user_fields'][$id] = array(
      'label' => array(
        '#markup' => $item['name'],
      ),
      'name' => array(
        '#type' => 'hidden',
        '#value' => $item['name'],
      ),
      'checked' => array(
        '#type' => 'checkbox',
        '#default_value' => $item['checked'],
      ),
      'op_edit' => array(
        '#markup' => $edit_link,
      ),
      'op_delete' => array(
        '#markup' => $delete_link,
      ),
      'id' => array(
        '#type' => 'hidden',
        '#default_value' => $item['id'],
      ),
      'pid' => array(
        '#type' => 'hidden',
        '#default_value' => $item['pid'],
      ),
      'weight' => array(
        '#type' => 'weight',
        '#default_value' => $item['weight'],
        '#delta' => 200,
        '#title_display' => 'invisible',
      ),
      'depth' => array(
        '#type' => 'hidden',
        '#value' => $item['depth'],
      ),
    );
    if (strpos($id, 'SECTION-') === 0) {
      $form['views_user_fields'][$id]['checked'] = array(
        '#markup' => '',
      );
    }
  }

  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );
  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));

  return $form;
}

/**
 * Theme callback for views_user_fields_admin_manage_fields_form().
 */
function theme_views_user_fields_admin_manage_fields_form($variables) {
  $form = $variables['form'];

  $rows = array();

  $class_weight = 'item-weight';
  $class_id = 'item-id';
  $class_pid = 'item-pid';
  foreach (element_children($form['views_user_fields']) as $id) {
    $class_row = array();
    $form['views_user_fields'][$id]['weight']['#attributes']['class'] = array($class_weight);
    $form['views_user_fields'][$id]['id']['#attributes']['class'] = array($class_id);
    $form['views_user_fields'][$id]['pid']['#attributes']['class'] = array($class_pid);


    $indent = theme('indentation', array('size' => $form['views_user_fields'][$id]['depth']['#value']));
    unset($form['views_user_fields'][$id]['depth']);

    if (strpos($id, 'SECTION-') === 0) {
      $class_row[] = 'tabledrag-root';
    }
    else {
      $class_row[] = 'tabledrag-leaf';
    }

    $class_row[] = 'draggable';
    $rows[] = array(
      'data' => array(
        $indent . drupal_render($form['views_user_fields'][$id]['label']),
        drupal_render($form['views_user_fields'][$id]['checked']),
        drupal_render($form['views_user_fields'][$id]['op_edit']),
        drupal_render($form['views_user_fields'][$id]['op_delete']),
        array(
          'data' => drupal_render($form['views_user_fields'][$id]['weight'])
          . drupal_render($form['views_user_fields'][$id]['id'])
          . drupal_render($form['views_user_fields'][$id]['pid']),
          'class' => array('tabledrag-hide'),
        )
      ),
      'class' => $class_row,
    );
  }

  $table_id = 'views-user-fields-admin-manage-fields';
  $output = drupal_render($form['title']);
  $output .= theme('table', array(
    'header' => array(
      t('Field'),
      t('Checked'),
      array('data' => t('Operation'), 'colspan' => 2),
      array('data' => t('Weight'), 'class' => array('tabledrag-hide')),
    ),
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag($table_id, 'match', 'parent', $class_pid, $class_pid, $class_id, FALSE);
  drupal_add_tabledrag($table_id, 'order', 'sibling', $class_weight, NULL, NULL, FALSE);

  return $output;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_form().
 */
function views_user_fields_admin_manage_fields_form_submit($form, &$form_state) {
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];
  $fields_conf = $form_state['values']['views_user_fields'];
  // Sort
  uasort($fields_conf, function($a, $b) {
    if ($a['weight'] == $b['weight']) {
      return 0;
    }
    return ($a['weight'] < $b['weight']) ? -1 : 1;
  });
  // Clean
  foreach ($fields_conf as $id => $item) {
    unset($fields_conf[$id]['depth']);
  }
  $confs = variable_get('views_user_fields_fields_confs', array());
  $confs[$view_name][$view_display_id][$conf_id]['fields'] = $fields_conf;
  variable_set('views_user_fields_fields_confs', $confs);
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_edit_section_form($form, &$form_state, $view_name, $view_display_id, $conf_id = 'default', $section_id = NULL) {

  $confs = variable_get('views_user_fields_fields_confs', array());
  $section = !empty($confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id]) ? $confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id] : array();

  $form['section_name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#description' => t('Name of the section.'),
    '#required' => TRUE,
  );
  if (!empty($section['name'])) {
    $form['section_name']['#default_value'] = $section['name'];
  }
  if ($section_id === NULL) {
    $op = 'add';
    $form['section_id'] = array(
      '#type' => 'machine_name',
      '#machine_name' => array(
        'source' => array('section_name'),
        'exists' => 'views_user_fields_fields_conf_section_id_exists',
      ),
    );
  }
  else {
    $op = 'edit';
    $form['section_id'] = array(
      '#type' => 'hidden',
      '#value' => $section_id,
    );
  }
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );
  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/conf/$conf_id";
  $form_state['storage']['op'] = $op;
  if (empty($section_id)) {
    $form = confirm_form($form, 'Add section', $form_state['storage']['redirect'], '', t('Save'));
  }
  else {
    $form = confirm_form($form, 'Edit section', $form_state['storage']['redirect'], '', t('Save'));
  }
  return $form;
}

/**
 * Machine name #exists callback.
 */
function views_user_fields_fields_conf_section_id_exists($section_id, $element, $form_state) {
  $out = FALSE;
  if ($form_state['storage']['op'] === 'add') {
    $view_name = $form_state['values']['view_name'];
    $view_display_id = $form_state['values']['view_display_id'];
    $conf_id = $form_state['values']['conf_id'];
    $section_id = 'SECTION-' . $section_id;
    $confs = variable_get('views_user_fields_fields_confs', array());
    $confs = !empty($confs[$view_name][$view_display_id]) ? $confs[$view_name][$view_display_id] : array();
    $out = !empty($confs[$conf_id]['fields'][$section_id]);
  }
  return $out;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_edit_section_form().
 */
function views_user_fields_admin_manage_fields_edit_section_form_submit($form, &$form_state) {

  $section_name = $form_state['values']['section_name'];
  $section_id = $form_state['values']['section_id'];
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];

  $confs = variable_get('views_user_fields_fields_confs', array());

  if ($form_state['storage']['op'] === 'add') {
    $section_id = 'SECTION-' . $section_id;
    $confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id] = array();
  }
  $confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id] += array(
    'id' => $section_id,
    'pid' => 0,
    'weight' => 0,
    'depth' => 0,
    'type' => 'section',
    'name' => $section_name,
  );
  $confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id]['name'] = $section_name;
  variable_set('views_user_fields_fields_confs', $confs);

  $form_state['redirect'] = $form_state['storage']['redirect'];
}

/**
 * hook_menu callback, see risappi_menu().
 */
function views_user_fields_admin_manage_fields_delete_section_form($form, &$form_state, $view_name, $view_display_id, $conf_id, $section_id) {

  $confs = variable_get('views_user_fields_fields_confs', array());
  $section = $confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id];
  $form['message'] = array(
    '#markup' => t('Delete %section_name', array('%section_name' => $section['name'])),
  );
  $form['section_id'] = array(
    '#type' => 'hidden',
    '#value' => $section_id,
  );
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['view_display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view_display_id,
  );
  $form['conf_id'] = array(
    '#type' => 'hidden',
    '#value' => $conf_id,
  );

  $form_state['storage']['redirect'] = "admin/views-user-fields/manage/$view_name/$view_display_id/conf/$conf_id/";
  $form = confirm_form($form, 'Delete section', $form_state['storage']['redirect'], '', t('Delete'));
  return $form;
}

/**
 * Submit callback for views_user_fields_admin_manage_fields_delete_section_form().
 */
function views_user_fields_admin_manage_fields_delete_section_form_submit($form, &$form_state) {

  $section_id = $form_state['values']['section_id'];
  $view_name = $form_state['values']['view_name'];
  $view_display_id = $form_state['values']['view_display_id'];
  $conf_id = $form_state['values']['conf_id'];

  $confs = variable_get('views_user_fields_fields_confs', array());
  unset($confs[$view_name][$view_display_id][$conf_id]['fields'][$section_id]);
  foreach ($confs[$view_name][$view_display_id][$conf_id]['fields'] as &$field) {
    if ($field['pid'] === $section_id) {
      $field['pid'] = 0;
    }
  }
  variable_set('views_user_fields_fields_confs', $confs);

  $form_state['redirect'] = $form_state['storage']['redirect'];
}
